<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.index');
})->name('inicio');

Route::get('dashboard', function () {
    return view('login.dashboard');
});
Route::get('welcome', function () {
    return view('welcome');
});
Route::post('ingresar','StaffController@index')->name('ingresar');
Route::get('facturar','StaffController@facturar')->name('caja');
Route::get('reportar','StaffController@reportar')->name('reporte');
Route::get('ver_staff','StaffController@getStaff')->name('gestion de usuarios');
Route::get('ver_privilegios','PrivilegeController@index')->name('gestion de permisos');
Route::get('ver_roles','StaffController@getRole')->name('gestion de roles');
Route::get('salir','StaffController@logOut')->name('salir');
//Done by CSR
Route::get('login', function () {
    return view('user.login');
});
Route::post('login','LoginController@login')->name('login');
Route::post('logout','LoginController@logout')->name('logout');
Route::get('at','RoleController@store');
Route::get('menu', function () {
    return view('activity.create');
});

//Resource routes for the activity controller
Route::resource('activities', 'ActivityController')->middleware('check_auth');

//Resource routes fot the role controller
Route::resource('roles', 'RoleController')->middleware('check_auth');

//Resource routes for the user controller
Route::resource('users', 'UserController')->middleware('check_auth');

//Route::resource('login','LoginController');


//Route::post('login','Auth\LoginController@login')->name('login');

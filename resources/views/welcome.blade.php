@extends ('layouts.app')
@section ('content')
    <div class="row">
        <div class="col-md-4 offset-md-1">
            <div class="panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Iniciar Sesión</h1>

                        </div>
                        <div class="panel-body">
                            <form method="POST" action="{{route('ingresar')}}" >
                                {{csrf_field()}}
                                <div class="form-group {{ $errors ->has('name') ? 'text-danger' : '' }}">
                                    <label for="name">Usuario:</label>
                                    <input class="form-control" type="text" name="name" placeholder="Ingresa tu nombre de usuario">
                                    {!! $errors->first('name','<span class="help-block">:message </span>') !!}
                                </div>
                                <div class="form-group {{$errors ->has('password') ? 'text-danger' : '' }}">
                                    <label for="password">Contraseña:</label>
                                    <input class="form-control" type="password" name="password" placeholder="Ingresa tu contraseña">
                                    {!! $errors->first('password','<span class="help-block">:message </span>') !!}
                                </div>
                                <button class="btn btn-primary btn-block">Acceder</button>
                            </form>
                        </div>
            </div>
         </div>
    </div>

@endsection

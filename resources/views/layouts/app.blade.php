<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
      <div class="container offset-md-4">
          <br><br><br><br>
          @yield('content')
      </div>
</body>
</html>
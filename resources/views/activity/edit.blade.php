@extends('main.main')

@section('title', 'Edición de Privilegios')
@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'activities.update', $activity->id ] ]) !!}
    <div class="form-group">
        <label for="privilege">Privilegio</label>
        <input type="text" class="form-control" name="activity_name"
               aria-describedby="activity_help" placeholder="Ingrese un privilegio"
               value="{{ $activity->activity_name }}">
        <small id="activity_help" class="form-text text-muted">Las opciones o funciones que se encontraran disponibles.</small>
    </div>
    <button type="submit" class="btn btn-primary">Actualizar</button>
    {!! Form::close() !!}
@endsection
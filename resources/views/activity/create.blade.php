@extends('main.main')

@section('title', 'Creación de Privilegios')
@section('content')
    {!! Form::open(['url' => 'activities']) !!}
    <div class="form-group">
        <label for="privilege">Privilegio</label>
        <input type="text" class="form-control" name="activity_name" aria-describedby="activity_help" placeholder="Ingrese un privilegio">
        <small id="activity_help" class="form-text text-muted">Las opciones o funciones que se encontraran disponibles.</small>
    </div>
    <button type="submit" class="btn btn-primary">Registrar</button>
    {!! Form::close() !!}
@endsection
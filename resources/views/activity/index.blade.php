@extends('main.main')

@section('title', 'Lista de Privilegios')

@section('add')

    <a href="{{ url('activities/create') }}"><button type="submit" class="btn btn-primary">Añadir</button></a>

@endsection

@section('content')

    <table class="table table-condensed">
        <tr>
            <th style="width: 10px">#</th>
            <th>Privilegio</th>
            <th style="width: 10px">Modificar</th>
            <th style="width: 10px">Eliminar</th>
        </tr>

        @foreach ($activities as $activity)
            <tr>
                <td></td>
                <td>{{ $activity->activity_name }}</td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'get', 'route' => [ 'activities.edit', $activity->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i></button>
                    {!! Form::close() !!}
                </td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'activities.destroy', $activity->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@endsection
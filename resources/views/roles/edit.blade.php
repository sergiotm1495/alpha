@extends('main.main')

@section('title', 'Edición de Roles')
@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'roles.update', $role->id ] ]) !!}
    <div class="form-group">
        <label for="role">Rol u ocupación</label>
        <input type="text" class="form-control" name="role_name" aria-describedby="role_help"
               value="{{ $role->role_name }}" placeholder="Ingrese un rol">
        <small id="role_help" class="form-text text-muted">Los roles u ocupaciones que tendra un usuario.</small>
    </div>

    <div class="form-group">
        <label for="role">Privilegios</label>
        <select multiple="multiple" class="form-control" name="activities[]" aria-describedby="activity_help">
            @foreach ($activities as $activity)
                <option value="{{ $activity->id }}" >{{ $activity->activity_name }}</option>
            @endforeach
        </select>
        <small id="activity_help" class="form-text text-muted">Los privilegios a los que accedera el usuario.</small>
    </div>


    <button type="submit" class="btn btn-primary">Actualizar</button>
    {!! Form::close() !!}
@endsection
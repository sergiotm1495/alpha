@extends('main.main')

@section('title', 'Rol u ocupación')
@section('content')
<div class="form-group">

    <label>El rol: {{ $role->role_name }} tiene privilegios para acceder a las siguientes funciones:</label>
    <ul>
        @foreach($activities as $activity)
            <li><label>{{ $activity->activity_name }}</label></li>
        @endforeach
    </ul>
</div>


@endsection
@extends('main.main')

@section('title', 'Lista de Roles')

@section('add')

    <a href="{{ url('roles/create') }}"><button type="submit" class="btn btn-primary">Añadir</button></a>

@endsection

@section('content')

    <table class="table table-condensed">
        <tr>
            <th style="width: 10px">#</th>
            <th>Rol</th>
            <th style="width: 10px">Ver</th>
            <th style="width: 10px">Modificar</th>
            <th style="width: 10px">Eliminar</th>
        </tr>

        @foreach ($roles as $role)
            <tr>
                <td></td>
                <td>{{ $role->role_name }}</td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'get', 'route' => [ 'roles.show', $role->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                    {!! Form::close() !!}
                </td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'get', 'route' => [ 'roles.edit', $role->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i></button>
                    {!! Form::close() !!}
                </td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'roles.destroy', $role->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@endsection
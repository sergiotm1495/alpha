@extends('main.main')

@section('title', 'Usuario')
@section('content')
    <div class="form-group">

        <label>El Usuario: {{ $user->user_name }} {{ $user->user_last_name }} tiene los siguientes cargos:</label>
        <ul>
            @foreach($roles as $role)
                <li><label>{{ $role->role_name }}</label></li>
            @endforeach
        </ul>
    </div>


@endsection
@extends('main.main')

@section('title', 'Edicion de Usuarios')
@section('content')
    {!! Form::open([ 'method'  => 'put', 'route' => [ 'users.update', $user->id ] ]) !!}
    <div class="form-group">
        <label for="user_name">Nombres</label>
        <input type="text" class="form-control" name="user_name" aria-describedby="user_name_help"
               value="{{ $user->user_name }}"
               placeholder="Ingrese los nombres del usuario">
        <small id="user_name_help" class="form-text text-muted">Nombres del usuario.</small>
    </div>
    <div class="form-group">
        <label for="user_last_name">Apellidos</label>
        <input type="text" class="form-control" name="user_last_name" aria-describedby="user_last_name_help"
               value="{{ $user->user_last_name }}"
               placeholder="Ingrese los apellidos del usuario">
        <small id="user_last_name_help" class="form-text text-muted">Apellidos del usuario.</small>
    </div>
    <div class="form-group">
        <label for="user_email">Correro Electronico</label>
        <input type="email" class="form-control" name="user_email" aria-describedby="user_email_help"
               value="{{ $user->user_email }}"
               placeholder="Ingrese el correo electronico del usuario">
        <small id="user_email_help" class="form-text text-muted">Correo electronico del usuario con el que podra ingresar al sistema.</small>
    </div>
    <div class="form-group">
        <label for="user_email">Contraseña</label>
        <input type="password" class="form-control" name="password" aria-describedby="user_user_password_help" placeholder="Ingrese la contraseña del usuario">
        <small id="user_password_help" class="form-text text-muted">Contraseña para ingresar al sistema.</small>
    </div>

    <div class="form-group">
        <label for="role">Roles o cargos</label>
        <select multiple="multiple" class="form-control" name="roles[]" aria-describedby="role_help">
            @foreach ($roles as $role)
                <option value="{{ $role->id }}">{{ $role->role_name }}</option>
            @endforeach
        </select>
        <small id="role_help" class="form-text text-muted">El o los cargos que asumira el usuario.</small>
    </div>


    <button type="submit" class="btn btn-primary">Actualizar</button>
    {!! Form::close() !!}
@endsection
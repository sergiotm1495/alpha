@extends('main.main')

@section('title', 'Lista de Usuarios')

@section('add')

    <a href="{{ url('users/create') }}"><button type="submit" class="btn btn-primary">Añadir</button></a>

@endsection

@section('content')

    <table class="table table-condensed">
        <tr>
            <th style="width: 10px">#</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Email</th>
            <th style="width: 10px">Ver</th>
            <th style="width: 10px">Modificar</th>
            <th style="width: 10px">Eliminar</th>
        </tr>

        @foreach ($users as $user)
            <tr>
                <td></td>
                <td>{{ $user->user_name }}</td>
                <td>{{ $user->user_last_name }}</td>
                <td>{{ $user->user_email }}</td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'get', 'route' => [ 'users.show', $user->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-eye"></i></button>
                    {!! Form::close() !!}
                </td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'get', 'route' => [ 'users.edit', $user->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i></button>
                    {!! Form::close() !!}
                </td>
                <td align="center">
                    {!! Form::open([ 'method'  => 'delete', 'route' => [ 'users.destroy', $user->id ] ]) !!}
                    <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i></button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
    </table>
@endsection

#Taller de Sistemas - Trabajo práctico

###Caracteristicas

* Proyecto: Mecanismo de Seguridad del Sistema

* Lenguaje de Programación: PHP 

* Framework: Laravel

* Motor de Base de Datos: Postgre Sql

* Idioma:Ingles

* Control de Versiones: GitLab

* SE NECESITA:

  * Php 7.1.8
  * Laravel 5.6
  * PostgreSQL 10.3
  
###Instalacion  del proyecto

Para ejecutar el proyecto se deben seguir los siguientes pasos:

1. git clone https://gitlab.com/sergiotm1495/alpha.git

1. $ composer install

2. $ cp .env.example .env

3. Crear bd en postgresql con nombre workshop_db

3. Editar el archivo .env con la informacion del DBMS y la BDD

4. $ php artisan key:generate

5. $ php artisan migrate

6. Para ejecutar el seeder:

    * $ composer dump-autoload
    * $ php arisan db:seed
    * $ php artisan db:seed --class=StaffTableSeeder
    
7. $ php artisan serve 



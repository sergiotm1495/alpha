<?php

use Illuminate\Database\Seeder;

class StaffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = [['name_staff' => 'admin','password_staff' => 'admin'],
                  ['name_staff' => 'cajero','password_staff' => 'cajero123'],
                  ['name_staff' => 'supervisor','password_staff' => 'super123']];
        $roles = [['name_role' => 'cajero'],['name_role' => 'supervisor'],['name_role' => 'administrador']];
        $priv = [['name_privileges' => 'caja'],['name_privileges' => 'reporte'],['name_privileges' => 'anular factura'],['name_privileges' => 'gestion de usuarios'],['name_privileges' => 'gestion de roles'],['name_privileges' => 'gestion de permisos']];
        $s_r = [['role_id'=>'1','staff_id'=>'2'],['role_id'=>'2','staff_id'=>'3'],['role_id'=>'3','staff_id'=>'1']];
        $p_r = [['role_id'=>'1','privilege_id'=>'1'],['role_id'=>'2','privilege_id'=>'1'],['role_id'=>'2','privilege_id'=>'2'],['role_id'=>'2','privilege_id'=>'3'],['role_id'=>'3','privilege_id'=>'4'],['role_id'=>'3','privilege_id'=>'5'],['role_id'=>'3','privilege_id'=>'6']];
        DB::table('staff')->insert($staff);
        DB::table('roles')->insert($roles);
        DB::table('privileges')->insert($priv);
        DB::table('role_privilege')->insert($p_r);
        DB::table('role_staff')->insert($s_r);
    }
}

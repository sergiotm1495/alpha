<?php

use Faker\Generator as Faker;

$factory->define(project\Role::class, function (Faker $faker) {
    return [
        'role_name' => $faker->colorName,
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(project\Activity::class, function (Faker $faker) {
    return [
        'activity_name' => $faker->jobTitle,
    ];
});

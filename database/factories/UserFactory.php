<?php

use Faker\Generator as Faker;

$factory->define(project\User::class, function (Faker $faker) {
    return [
        'user_name' => $faker->name,
        'user_last_name' => $faker->name,
        'user_email' => $faker->email,
        'password' => bcrypt('secret'),
    ];
});

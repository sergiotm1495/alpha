<?php

namespace project;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Auth\Authenticatable;
class User extends Model implements AuthenticatableContract
{
    use Authenticatable;
    protected $fillable = ['user_name', 'user_last_name', 'user_email', 'password'];

    public function roles()
    {
        return $this->belongsToMany('project\Role');
    }
}

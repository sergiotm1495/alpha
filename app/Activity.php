<?php

namespace project;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = ['activity_name'];

    public function roles()
    {
        return $this->belongsToMany('project\Role');
    }
}

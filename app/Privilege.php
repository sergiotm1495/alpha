<?php

namespace project;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    protected $table='privileges';
    protected $primaryKey='id';
    public $timestamps=false;

    protected $fillable =[
        'name_privileges',
    ];

    protected $guarded =[

    ];
}

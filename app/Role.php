<?php

namespace project;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role_name'];

    public function users()
    {
        return $this->belongsToMany('project\User');
    }
    public function activities()
    {
        return $this->belongsToMany('project\Activity');
    }
}

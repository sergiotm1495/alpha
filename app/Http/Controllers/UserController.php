<?php

namespace project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use project\Http\Requests\UserFormRequest;
use project\Role;
use project\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('user.create')->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserFormRequest $request)
    {
        //dd($request->all());
        DB::transaction(function() use ($request) {
            $user = new User($request->all());
            $user->password = bcrypt($request->password);
            $user->save();
            $user->roles()->sync($request->get('roles'));
        });


        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $roles = $user->roles()->get()->all();
        return view('user.show')->with('roles', $roles)->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        //dd($role->activities()->get()->all());
        //$activities = $role->activities()->get()->all();
        $roles = Role::all();
        return view('user.edit')->with('roles', $roles)->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(UserFormRequest $request, User $user)
    {
        DB::transaction(function() use ($request, $user) {
            $user->update($request->all());
            $user->password = bcrypt($request->password);
            $user->save();
            $user->roles()->sync($request->get('roles'));
        });

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::transaction(function() use ($user) {
            $user->roles()->detach();
            $user->delete();
        });

        return redirect('users');
    }
}

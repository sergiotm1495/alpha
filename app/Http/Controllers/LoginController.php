<?php

namespace project\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        //dd($request->all());
        if (Auth::attempt(['user_email' => $request->email, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->intended('users');
        }
        else
        {
            dd("nono");
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

}

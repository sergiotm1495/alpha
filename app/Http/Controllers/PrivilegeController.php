<?php

namespace project\Http\Controllers;

use project\Privilege;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use project\Http\Requests\PrivilegeFormRequest;
use DB;

class PrivilegeController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $privileges=DB::table('privileges')->where('name_privileges','LIKE','%'.$query.'%')
                ->orderBy('id','asc')
                ->paginate(7);
            return view('privilege.index',['privileges'=>$privileges,'nombre'=>$query]);
        }
    }
    public function create()
    {
        return view("privilege.create");
    }
    public function store(PrivilegeFormRequest $request)
    {
        $privilege=new Privilege;
        $privilege->name_privileges=$request->get('name_privileges');
        $privilege->save();
        return Redirect::to('privilege');
    }
    public function show($id)
    {
        return view("privilege.show",["privilege"=>Privilege::findOrFail($id)]);
    }
    public function edit($id)
    {
        return view("privilege.edit",["privilege"=>Privilege::findOrFail($id)]);
    }
    public function update(PrivilegeFormRequest $request,$id)
    {
        $privilege=Privilege::findOrFail($id);
        $privilege->name_privileges=$request->get('name_privileges');
        $privilege->update();
        return Redirect::to('privilege');
    }
    public function destroy($id)
    {
        $privilege=Privilege::findOrFail($id);
        $privilege->update();
        return Redirect::to('privilege');
    }

}

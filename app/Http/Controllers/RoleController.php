<?php

namespace project\Http\Controllers;

use Illuminate\Support\Facades\DB;
use project\Activity;
use project\Http\Requests\RoleFormRequest;
use project\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activities = Activity::all();

        return view('roles.create')->with('activities', $activities);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleFormRequest $request)
    {
        DB::transaction(function() use ($request) {
            $role = new Role($request->all());
            $role->save();
            $role->activities()->sync($request->get('activities'));
        });


        return redirect('roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $activities = $role->activities()->get()->all();
        return view('roles.show')->with('role', $role)->with('activities', $activities);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {

        //dd($role->activities()->get()->all());
        //$activities = $role->activities()->get()->all();

        $activities = Activity::all();
        return view('roles.edit')->with('role', $role)->with('activities', $activities);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleFormRequest $request, Role $role)
    {
        DB::transaction(function() use ($request, $role) {
            $role->update($request->all());
            $role->save();
            $role->activities()->sync($request->get('activities'));
        });

        return redirect('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \project\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        DB::transaction(function() use ($role) {
            $role->activities()->detach();
            $role->users()->detach();
            $role->delete();
        });

        return redirect('roles');
    }
}

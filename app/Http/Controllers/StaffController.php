<?php

namespace project\Http\Controllers;

use project\Staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $this->validate(request(),[
            'name' => 'required|string',
            'password' => 'required|string'
        ]);


        $nombre = $request->input('name');
        $pass = $request->input('password');

        $personal = DB::select("select * from staff a where a.name_staff='".$nombre."' and a.password_staff='".$pass."'");
        $entra = count($personal)==1?true:false;


        if($entra)
        {
            $id = "";
            $nombre_user = "";
            foreach ($personal as $per)
            {
                $id = $per->id;
                $nombre_user = $per->name_staff;
            }

            $accesos = DB::select('select b.name_privileges as "name"
                                   from staff a, privileges b, roles c, role_privilege d, role_staff e  
                                   where a.id = e.staff_id and c.id = e.role_id  and b.id = d.privilege_id and c.id = d.role_id and  a.id = ?', [$id]);
            $opciones=array();
            foreach ($accesos as $ac)
            {
               array_push( $opciones,$ac->name);
            }

            session([
                'nombre' => $nombre_user,
                'privilegios'=> $opciones
            ]);
            return view("login.dashboard",['opciones' => session('privilegios'),'nombre' => session('nombre')]);

        }
        else {
            return view('login.index');
        }

    }

    public function facturar(){
        if(session()->has('nombre'))
        {
            return view('login.caja', ['opciones' => session('privilegios'), 'nombre' => session('nombre')]);
        }else{
            return view('error');
        }
    }
    public function reportar()
    {
        if(session()->has('nombre'))
        {
            return view('login.reporte', ['opciones' => session('privilegios'), 'nombre' => session('nombre')]);
        }
        else{
            return view('error');
        }
    }
    public function getStaff(){
        if(session()->has('nombre'))
        {
            return view('staff.index', ['opciones' => session('privilegios'), 'nombre' => session('nombre')]);
        }
        else{
            return view('error');
        }
    }
    public function getPrivilege(){
        if(session()->has('nombre'))
        {
            return view('privilege.index', ['opciones' => session('privilegios'), 'nombre' => session('nombre')]);
        }
        else{
            return view('error');
        }
    }
    public function getRole(){
        if(session()->has('nombre'))
        {
            return view('roles.index', ['opciones' => session('privilegios'), 'nombre' => session('nombre')]);
        }
        else{
            return view('error');
        }
    }
    public function logOut(){
        session()->flush();
        return redirect()->route('inicio');

    }
}

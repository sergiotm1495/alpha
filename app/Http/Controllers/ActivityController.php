<?php

namespace project\Http\Controllers;

use Illuminate\Support\Facades\DB;
use project\Activity;
use Illuminate\Http\Request;
use project\Http\Requests\ActivityFormRequest;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $activities = Activity::all();

        return view('activity.index')->with('activities', $activities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityFormRequest $request)
    {
        //dd($request->all());
        DB::transaction(function() use ($request) {
            $activity = new Activity($request->all());
            $activity->save();

        });
        return redirect('activities');
    }

    /**
     * Display the specified resource.
     *
     * @param  \project\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \project\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        return view('activity.edit')->with('activity', $activity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \project\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activity $activity)
    {
        DB::transaction(function() use ($request, $activity) {
            $activity->update($request->all());
            $activity->save();

        });
        return redirect('activities');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \project\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
        DB::transaction(function() use ($activity) {
            $activity->roles()->detach();
            $activity->delete();

        });
        return redirect('activities');
    }
}
